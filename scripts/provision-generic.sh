#!/bin/sh

set -eux

# Install qemu-guest-agent for graceful libvirt shutdown
apk upgrade -Ua
apk add qemu-guest-agent
rc-update add qemu-guest-agent

# Zero out empty space (slower build) to make the result image a little bit smaller
dd if=/dev/zero of=/zero.fill bs=1M || true && sync && sleep 1 && sync && rm -f /zero.fill

# Discard unused blocks
fstrim -v /
